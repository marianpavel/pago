package ro.marianpavel.testpago.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.adapter_page_two.view.*
import ro.marianpavel.testpago.R
import ro.marianpavel.testpago.pojo.Location
import ro.marianpavel.testpago.pojo.Walk

class PageTwoAdapter(private var list: MutableList<Any>)
    : RecyclerView.Adapter<PageTwoAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): PageTwoAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_page_two, parent, false) as View

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (list[position] is Walk) {
            val walk = list[position] as Walk
            holder.view.id_holder.text = walk.id.toString()
            holder.view.title_holder.text = walk.title
        }

        if (list[position] is Location) {
            val location = list[position] as Location
            holder.view.id_holder.text = location.id.toString()
            holder.view.title_holder.text = location.title
        }
    }

    override fun getItemCount() = list.size
}