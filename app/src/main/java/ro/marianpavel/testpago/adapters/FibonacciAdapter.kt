package ro.marianpavel.testpago.adapters

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.adapter_fibonacci.view.*
import ro.marianpavel.testpago.interfaces.FibonacciListener
import ro.marianpavel.testpago.R
import ro.marianpavel.testpago.pojo.FibonacciNumber
import ro.marianpavel.testpago.utils.FibonacciDifUtil
import java.math.BigInteger
import java.util.*

class FibonacciAdapter(private var context: Context,
                       private var list: MutableList<FibonacciNumber>,
                       private var listener: FibonacciListener) :
        RecyclerView.Adapter<FibonacciAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): FibonacciAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_fibonacci, parent, false) as View

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var number = list[holder.adapterPosition]
        holder.view.fibonacci_text.text = String.format(Locale.GERMAN, "%s",
                number.number)

        if (list[position].isPrime) {
            holder.view.button.visibility = View.VISIBLE
            holder.view.checkbox.visibility = View.VISIBLE
            holder.view.checkbox.isChecked = number.isChecked
            holder.view.image.visibility = View.GONE
            Glide.with(context).clear(holder.view.image)
            holder.view.image.setImageDrawable(null)

            holder.view.checkbox.setOnCheckedChangeListener { compoundButton, b ->
                number.isChecked = b
            }

            holder.view.button.setOnClickListener {
                if (holder.view.checkbox.isChecked) {
                    listener.onNavigateToPageTwo(position, holder.view.fibonacci_text)
                } else {
                    Toast.makeText(context, context.getString(R.string.sad_message), Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            holder.view.button.visibility = View.GONE
            holder.view.checkbox.visibility = View.GONE
            holder.view.image.visibility = View.VISIBLE
            Glide.with(context)
                    .load("https://picsum.photos/200/200?image=" + Random().nextInt(1000))
                    .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                    .into(holder.view.image)
        }
    }

    override fun getItemCount() = list.size
}