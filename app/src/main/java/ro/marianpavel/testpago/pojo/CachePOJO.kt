package ro.marianpavel.testpago.pojo

import io.realm.RealmObject
import io.realm.annotations.RealmClass

class CachePOJO: RealmObject() {
    var id: Int = 0
    var title: String = ""
}