package ro.marianpavel.testpago.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Location {

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("title")
    @Expose
    lateinit var title: String

    @SerializedName("location")
    @Expose
    lateinit var location: String
}