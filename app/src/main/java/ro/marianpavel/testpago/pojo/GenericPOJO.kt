package ro.marianpavel.testpago.pojo

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


class GenericPOJO<T> {

    @Expose
    @SerializedName("results")
    var `object`: T? = null
}