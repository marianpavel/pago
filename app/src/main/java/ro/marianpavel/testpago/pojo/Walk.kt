package ro.marianpavel.testpago.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Walk {

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("title")
    @Expose
    var title: String = ""

    @Expose
    @SerializedName("places")
    lateinit var places: MutableList<String>
}