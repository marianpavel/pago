package ro.marianpavel.testpago.pojo

import java.math.BigInteger

class FibonacciNumber(var number: BigInteger) {
    var isPrime: Boolean = false
    var isChecked: Boolean = false
}