package ro.marianpavel.testpago.interfaces

import android.widget.TextView

interface FibonacciListener {
    fun onNavigateToPageTwo(position: Int, view: TextView)
}