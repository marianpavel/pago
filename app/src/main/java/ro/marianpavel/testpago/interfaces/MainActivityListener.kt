package ro.marianpavel.testpago.interfaces

interface MainActivityListener {

    fun onFibonacciAdded(start: Int)
    fun showProgress()
    fun hideProgress()

}