package ro.marianpavel.testpago.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Debug
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_page_two.*
import ro.marianpavel.testpago.R
import ro.marianpavel.testpago.adapters.PageTwoAdapter
import ro.marianpavel.testpago.interfaces.PageTwoListener
import ro.marianpavel.testpago.presenters.PageTwoPresenter
import ro.marianpavel.testpago.utils.Constants

class PageTwo : AppCompatActivity(), PageTwoListener {

    private lateinit var viewManager: LinearLayoutManager
    private lateinit var viewAdapter: PageTwoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_two)

        if (intent.getStringExtra(Constants.Bundle.PARAM_NUMBER) != null) {
            toolbar_title.text = intent.getStringExtra(Constants.Bundle.PARAM_NUMBER)
        }

        val pageTwoPresenter = PageTwoPresenter(this)
        pageTwoPresenter.makeAPICalls()
    }

    override fun onItemsready(locaionAndWalk: MutableList<Any>) {
        viewAdapter = PageTwoAdapter(locaionAndWalk)
        viewManager = LinearLayoutManager(applicationContext)
        recycler_view.apply {
            adapter = viewAdapter
            layoutManager = viewManager
        }
    }
}
