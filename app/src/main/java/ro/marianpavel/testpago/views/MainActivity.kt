package ro.marianpavel.testpago.views

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import ro.marianpavel.testpago.adapters.FibonacciAdapter
import ro.marianpavel.testpago.interfaces.FibonacciListener
import ro.marianpavel.testpago.presenters.MainActivityPresenter
import ro.marianpavel.testpago.R
import ro.marianpavel.testpago.pojo.FibonacciNumber
import ro.marianpavel.testpago.utils.Constants
import java.math.BigInteger
import android.support.v4.view.ViewCompat
import android.support.v4.app.ActivityOptionsCompat
import android.widget.TextView
import io.realm.Realm
import ro.marianpavel.testpago.interfaces.MainActivityListener


class MainActivity : AppCompatActivity(), FibonacciListener, MainActivityListener {

    private lateinit var scrollListener: RecyclerView.OnScrollListener
    private lateinit var viewManager: LinearLayoutManager
    private lateinit var viewAdapter: FibonacciAdapter
    private lateinit var list: MutableList<FibonacciNumber>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Realm.init(this)

        val activityPresenter = MainActivityPresenter(this)

        list = mutableListOf(
                FibonacciNumber(BigInteger("0")),
                FibonacciNumber(BigInteger("1")))

        activityPresenter.populateList(list)
        viewAdapter = FibonacciAdapter(applicationContext, list, this)
        viewManager = LinearLayoutManager(applicationContext)
        recycler_view.apply {
            adapter = viewAdapter
            layoutManager = viewManager
        }

        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView!!.layoutManager.itemCount
                if (totalItemCount == viewManager.findLastVisibleItemPosition() + 1 &&
                        progress.visibility == View.GONE) {
                    activityPresenter.populateList(list)
                }
            }
        }
        recycler_view.addOnScrollListener(scrollListener)
    }

    override fun onFibonacciAdded(start: Int) {
        viewAdapter.notifyItemRangeInserted(start, list.size - 1)
    }

    override fun onNavigateToPageTwo(position: Int, view: TextView) {
        intent = Intent(this, PageTwo::class.java)
        intent.putExtra(Constants.Bundle.PARAM_NUMBER, list[position].number.toString())

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@MainActivity,
                view,
                ViewCompat.getTransitionName(view))

        startActivity(intent, options.toBundle())
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }
}
