package ro.marianpavel.testpago.utils

import android.support.v7.util.DiffUtil
import java.math.BigInteger

class FibonacciDifUtil(private val oldList: MutableList<BigInteger>,
                       private val newList: MutableList<BigInteger>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].equals(newList[newItemPosition])
    }

}