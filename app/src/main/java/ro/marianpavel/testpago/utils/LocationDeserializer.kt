package ro.marianpavel.testpago.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ro.marianpavel.testpago.pojo.Location
import java.lang.reflect.Type

class LocationDeserializer: JsonDeserializer<Location> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Location {
        val location = Location()
        if (json != null && json.isJsonObject) {
            val obj = json.asJsonObject
            location.id = obj.get("id").asInt
            location.title = obj.get("title").asString
            location.location = obj.get("address").asString
        }
        return location
    }

}