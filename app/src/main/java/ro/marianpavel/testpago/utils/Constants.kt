package ro.marianpavel.testpago.utils

import ro.marianpavel.testpago.BuildConfig

class Constants {

    object Caching {
        @JvmStatic
        val CACHE_SIZE = 9
    }

    object Bundle {
        @JvmStatic
        val PARAM_NUMBER = BuildConfig.APPLICATION_ID + ".PARAM_NUMBER"
    }

    object Debug {
        @JvmStatic
        val TAG = "Marian"
    }
}