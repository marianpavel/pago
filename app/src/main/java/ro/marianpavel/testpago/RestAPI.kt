package ro.marianpavel.testpago

import retrofit2.http.GET
import io.reactivex.Observable
import ro.marianpavel.testpago.pojo.Location
import ro.marianpavel.testpago.pojo.Walk

interface RestAPI {

    @GET("locations")
    fun getLocations(): Observable<MutableList<Location>>

    @GET("walks")
    fun getWalks(): Observable<MutableList<Walk>>
}