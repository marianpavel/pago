package ro.marianpavel.testpago.presenters

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ro.marianpavel.testpago.interfaces.FibonacciListener
import ro.marianpavel.testpago.interfaces.MainActivityListener
import ro.marianpavel.testpago.pojo.FibonacciNumber
import ro.marianpavel.testpago.utils.Constants
import java.math.BigInteger

class MainActivityPresenter(var listener: MainActivityListener) {

    fun populateList(list: MutableList<FibonacciNumber>) {
        listener.showProgress()
        Observable.fromCallable<Any> {
            for (i in 0..Constants.Caching.CACHE_SIZE) {
                val number = FibonacciNumber(list[list.size -1].number.add(list[list.size - 2].number))
                number.isPrime = returnPrime(number.number)
                list.add(number)
            }
            true
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result ->
                    listener.hideProgress()
                    listener.onFibonacciAdded(list.size)
                }
    }

    private fun returnPrime(number: BigInteger): Boolean {
        return number.isProbablePrime(100)
    }
}