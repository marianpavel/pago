package ro.marianpavel.testpago.presenters

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import ro.marianpavel.testpago.interfaces.PageTwoListener
import ro.marianpavel.testpago.models.RestClient
import ro.marianpavel.testpago.pojo.CachePOJO
import ro.marianpavel.testpago.pojo.Location
import ro.marianpavel.testpago.pojo.LocationAndWalk
import ro.marianpavel.testpago.pojo.Walk
import ro.marianpavel.testpago.utils.Constants

class PageTwoPresenter(var listener: PageTwoListener) {

    private lateinit var realm: Realm

    fun makeAPICalls() {
//                    realm = Realm.getDefaultInstance()
//
//                    var cache = realm.where(CachePOJO::class.java).findAll()

//                    if (cache.size > 0) {
//                        //return cache
//                    } else {
                    val locations : Observable<MutableList<Location>> = RestClient.instance.api!!.getLocations()

                    val walks : Observable<MutableList<Walk>> = RestClient.instance.api!!.getWalks()

                    var combined : Observable<LocationAndWalk> = Observable.zip(locations, walks,
                            BiFunction { t1, t2 -> combineResults(t1, t2)})

                    combined.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            val tempList: MutableList<Any> = mutableListOf()

                            for (location in it.locations) {
                                if (location.id % 2 != 0) {
                                    tempList.add(location)
                                }
                            }

                            for (walk in it.walks) {
                                if (walk.id % 2 != 0) {
                                    tempList.add(walk)
                                }
                            }
                            listener.onItemsready(tempList)
                        }

//                    realm.beginTransaction()
//
//                    for (item in tempList) {
//                        val cache = CachePOJO()
//                        if (item is Location) {
//                            cache.id = item.id
//                            cache.title = item.title
//                        }
//
//                        if (item is Walk) {
//                            cache.id = item.id
//                            cache.title = item.title
//                        }
//
//                        realm.insert(cache)
//                    }
//
//                    realm.commitTransaction()
                    Log.i(Constants.Debug.TAG, "Transaction done")
//                }
    }

    private fun combineResults(t1: MutableList<Location>, t2: MutableList<Walk>): LocationAndWalk {
        return LocationAndWalk(t1, t2)
    }
}