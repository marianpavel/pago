package ro.marianpavel.testpago.models

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ro.marianpavel.testpago.BuildConfig
import ro.marianpavel.testpago.RestAPI
import ro.marianpavel.testpago.pojo.Location
import ro.marianpavel.testpago.utils.LocationDeserializer
import java.util.concurrent.TimeUnit


class RestClient private constructor() {
    var api: RestAPI? = null
        private set

    private fun createClient() {


        val builder = OkHttpClient().newBuilder()
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.connectTimeout(30, TimeUnit.SECONDS)


        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(interceptor)

        builder.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .build()
            chain.proceed(request)
        }

        val client = builder.build()

        val gson = GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .serializeNulls()
                .setPrettyPrinting()
                .registerTypeAdapter(Location::class.java, LocationDeserializer())
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        api = retrofit.create(RestAPI::class.java)
    }

    companion object {

        private var ourInstance: RestClient? = null

        val instance: RestClient
            get() {
                if (ourInstance == null) {
                    ourInstance = RestClient()
                    ourInstance!!.createClient()
                }

                return ourInstance as RestClient
            }
    }
}